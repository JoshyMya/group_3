import java.util.Scanner;
import java.util.regex.Pattern;

public class Logintesting {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		/*Logins to use!!!
		 * USERNAME: johni
		 * USERNAME: jacki
		 *USERNAME: timmi
		 */
		
		
		
		String registerusername = "";
		String registerpassword = "";
		String FirstName = null;
		String LastName= null;
		
		System.out.print("Do you have a registration? Enter 1 if you need to register or 2 if you need to login : ");
		int regis = scanner.nextInt();
		{
				// The name of new user.
					
					System.out.println("Please enter your name:");
						FirstName= scanner.next();
					System.out.println("Please enter your last name:");
						LastName= scanner.next();
		}
		      
			  //username registration
			  
		if(regis == 1)
		{
			boolean validation = false;
			
			while(!validation)
			{
				System.out.print("Please enter a username: ");
				registerusername = scanner.next();
				
				validation = isValid(registerusername, 'u');
				if(validation == false)
				{
		        System.out.println("Your username can only contain alphabets, numbers and, minimum of 5 and a maximum of 25 characters.\n");
				}
			}
			validation = false;
			
			     //passwoard registration 
				 
			while(!validation)
			{
				System.out.print("Please enter a password: ");
				registerpassword = scanner.next();
	
				validation = isValid(registerpassword, 'p');
				if(validation == false)
				{
		        System.out.println("Your password must containat least one upper case alphabets, one lower case alphabet, one numbers, and one special characters. At least a minmun of 8 and a maximum of 30 characters.\n");
				}
			}
			{
				System.out.println("Thank you for regestreing,");
				}
			regis = 2;
		}
		      //login process (username)
			  
		if (regis == 2) {
			System.out.println("please login\n");
			
			String username= "";
			while(!username.equals(registerusername))
			{
				System.out.println("please enter your username: ");
				username = scanner.next();
			
				if(!username.equals(registerusername))
				{
					System.out.println("Invalid Username\n");
				}
			}
			   //login process (password)
			   
			String password = "";
			
			while(!password.equals(registerpassword))
			{
				System.out.println("please enter your password: ");
				password = scanner.next();
				
				if(!password.equals(registerpassword))
				{
					System.out.println("Invalid Password\n");
				}
			}
			System.out.println(" Welome to The Smartboard ");
			
		}	
		
		
			//Directory
			int directnum = 0;
			do {
			System.out.println("Please enter the corresponding number to get the right application.");
			System.out.println("1 : What is Smartboard?\n2 : Advisor names\n3 : Courses currently taking\n4 : Grades\n5 : End the program");
			directnum = scanner.nextInt();
			
			//Summary
			if (directnum == 1) {
				System.out.println("Smartboard is a program designed to assist students in school by giving them easy access to certain information.");
				
			}
			//Advisor names
			else if (directnum == 2) {
				
				System.out.println("A-L : Joe Clark\nM-T : Maria Torres\nU-Z : Eric Ramirez ");
				
			}
			//Classes
			else if (directnum == 3) {
				if (registerusername.equals("johni")) {
					
					System.out.println("Class: Business 101    Teacher: John Smith         Time: MW 5:30pm- 7:00pm");
					System.out.println("Class: Biology         Teacher: Steve Johnson      Time: W 1:30pm- 5:00pm");
					
				}
				if (registerusername.equals("jacki")) {
					
					System.out.println("Class: Accounting 101    Teacher: Tom Stevenson       Time: MW 9:30am- 11:30am");
					System.out.println("Class: Business 101      Teacher: Steve Johnson       Time: MW 12:00pm- 1:45pm");
					System.out.println("Class: Calculus          Teacher: Martha Martinez     Time: MW 2:00pm- 4:00pm");
				}
				if (registerusername.equals("timmi")) {
					System.out.println("You have not registered for courses this semester.\nPLease see your advisor for more info.");
					         
				}
					
			}
			else if (directnum == 4) {
				if (registerusername.equals("johni")) {
					
					System.out.println("Business 101:   91");
					System.out.println("Biology :       71");
					
				}
				if (registerusername.equals("jacki")) {
					
					System.out.println("Accounting 101:   85");
					System.out.println("Business 101:     62");
					System.out.println("Calculus :        71");
				}
				if (registerusername.equals("timmi")) {
					System.out.println("You have not registered for courses this semester.\nPLease see your advisor for more info.");
					         
				}
				
			}
			else if(directnum == 5) {
				
				System.out.println("Thank you for using Smartboard!");
			}
			else {
				System.out.println("Invalid input, please put in a correct number");
				
			}
			}while (directnum != 5);
					
			
	}	

	
		
		
		
		
	
	         //valdation of username and password requirements
			 
	public static boolean isValid(String username, char t) 
	{ 
		String loginRegex; 
		
		if(t == 'u') loginRegex = "^[a-zA-Z0-9]{5,25}$";
		else loginRegex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])"+ "(?=.*[@#$%^&+=])" + "(?=\\S+$).{8,30}$";
							
		Pattern pat = Pattern.compile(loginRegex); 
		if (username == null) 
			return false; 
		return pat.matcher(username).matches(); 
	} 
}
	
	

	
