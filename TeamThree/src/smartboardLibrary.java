import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Library2 {
	
static ArrayList<Student> studentList=new ArrayList<Student>();
static ArrayList<Book> bookList=new ArrayList<Book>();
static ArrayList<Book> borroeList=new ArrayList<Book>();

   public static void main(String[] args) throws NumberFormatException, IOException {
       BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
       int reapeat=9;
       while(reapeat>0) {
          
           System.out.println("------------------Please choos below Option-----------");
           System.out.println("1. Add Student");
           System.out.println("2. Add Book");
           System.out.println("3. Display All Students");
           System.out.println("4. Display All Books");
           System.out.println("5. Search Student");
           System.out.println("6. Search Book");
           System.out.println("7. Borow Book");
           System.out.println("8. Display All Borow Book");
           System.out.println("9. Return Book");
           System.out.println("10. Exit");
           int option=Integer.parseInt(br.readLine());
           switch (option) {
           case 1:
           System.out.println("Enter Student Id");
           int id=Integer.parseInt(br.readLine());
           boolean chekId=checkStudentId(id);
           if(chekId) {
               System.out.println("StudentId Already Exist");
               break;
           }
           System.out.println("Enter Student surName");
           String surname=br.readLine();
           System.out.println("Enter Student lastName");
           String lastName=br.readLine();
           System.out.println("Enter Student Age");
           int age=Integer.parseInt(br.readLine());
           System.out.println("Enter Student Sex :");
           String sex=br.readLine();
           Student student=new Student(id, surname, lastName, age, sex);
           studentList.add(student);
           break;
           case 2:
               System.out.println("Enter Book Id :");
               int bookId=Integer.parseInt(br.readLine());
               boolean chekBookId=checkBookId(bookId);
               if(chekBookId) {
                   System.out.println("Book Id Already Exist");
                   break;
               }
               System.out.println("Enter Book tittle");
               String tittle=br.readLine();
               System.out.println("Enter uthor surName");
               String auhtorSurname=br.readLine();
               System.out.println("Enter Student lastName");
               String authorName=br.readLine();
               System.out.println("Enter Book Purchase Date");
               String purchaseDate=br.readLine();
               System.out.println("Enter Book Availability :");
               String status=br.readLine();
               Book b=new Book(bookId, tittle, auhtorSurname, authorName, purchaseDate, status);
               bookList.add(b);
           break;
           case 3:
           displayAllStudent();
           break;
           case 4:
           displayAllBook();
           break;
           case 5:
           System.out.println("Please Enter student Id, you want to Search");
           int searchStudentId=Integer.parseInt(br.readLine());
           searchStudent(searchStudentId);
           break;
           case 6:
               System.out.println("Please Enter Book Id, you want to Search");
               int searchBookId=Integer.parseInt(br.readLine());
               searchBook(searchBookId);
           break;
           case 7:
           borrowBook();
           break;
           case 8:
               DisplayborrowBook();
               break;
           case 9:
               returnBook();
               break;
           case 10:
               reapeat=-10;
               break;
           }
          
       }

   }
   private static void returnBook() throws NumberFormatException, IOException {
       BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
       System.out.println("---------------------");
       System.out.println("Please enter Student Id:");
       int id=Integer.parseInt(br.readLine());
       System.out.println("Please enter Book Id:");
       int bookId=Integer.parseInt(br.readLine());
       for(int i=0; i<borroeList.size(); i++) {
               if(borroeList.get(i).getBookId()==bookId){
                      
                   borroeList.get(i).setStatus("AVAILABLE");
                   borroeList.remove(i);
                   break;
               }
           }
         
      
   }
   private static void DisplayborrowBook() {
       for(Book book :borroeList) {
          
               System.out.println("Book Id:"+book.getBookId()+" Tittle :"+book.getTittle()+" Auhtor Surname:"+book.getAuthorSurname()+
                       " Author FirstName:"+book.getAuthorFirsName()+" Purchase Date:"+book.getPurchaseDate()+" staus:"+book.getStatus());
              
          
       }
   }
   private static void borrowBook() throws NumberFormatException, IOException {
       BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
       System.out.println("----List of all Available book as Below----");
       for(Book book :bookList) {
           if(book.getStatus().equalsIgnoreCase("AVAILABLE")) {
               System.out.println("Book Id:"+book.getBookId()+" Tittle :"+book.getTittle()+" Auhtor Surname:"+book.getAuthorSurname()+
                       " Author FirstName:"+book.getAuthorFirsName()+" Purchase Date:"+book.getPurchaseDate()+" staus:"+book.getStatus());
              
           }
       }
       System.out.println("---------------------");
       System.out.println("Please enter Student Id:");
       int id=Integer.parseInt(br.readLine());
       System.out.println("Please enter Book Id:");
       int bookId=Integer.parseInt(br.readLine());
       for(Book book :bookList) {
               if(book.getBookId()==bookId){
                       borroeList.add(book);
                       book.setStatus("Not AVAILABLE");
               }
           }
   }
   private static void searchBook(int searchBookId) {
       boolean search=true;
       for(Book book :bookList) {
           if(book.getBookId()==searchBookId) {
               System.out.println("Book Id:"+book.getBookId()+" Tittle :"+book.getTittle()+" Auhtor Surname:"+book.getAuthorSurname()+
                       " Author FirstName:"+book.getAuthorFirsName()+" Purchase Date:"+book.getPurchaseDate()+" staus:"+book.getStatus());
               search=false;
           }
          
       }
       if(search) {
           System.out.println(searchBookId+" Not exist");
       }
   }
   private static void searchStudent(int searchStudentId) {
       boolean search=true;
       for(Student st :studentList) {
           if(st.getStudentId()==searchStudentId) {
               System.out.println("Student Id:"+st.getStudentId()+" SurName:"+st.getSurname()+" lastName:"+st.getLastName()+
                       "Age:"+st.getAge()+" Sex:"+st.getSex());
               search=false;
           }
          
       }
      
       if(search) {
           System.out.println(searchStudentId+" Not exist");
       }
      
   }
   private static void displayAllBook() {
       for(Book book :bookList) {
           System.out.println("Book Id:"+book.getBookId()+" Tittle :"+book.getTittle()+" Auhtor Surname:"+book.getAuthorSurname()+
                   " Author FirstName:"+book.getAuthorFirsName()+" Purchase Date:"+book.getPurchaseDate()+" staus:"+book.getStatus());
       }
      
   }
   private static void displayAllStudent() {
       for(Student st :studentList) {
           System.out.println("Student Id:"+st.getStudentId()+" SurName:"+st.getSurname()+" lastName:"+st.getLastName()+
                   "Age:"+st.getAge()+" Sex:"+st.getSex());
       }
      
   }
   private static boolean checkBookId(int bookId) {
       for(Book book: bookList) {
           if(book.getBookId()==bookId) {
               return true;
           }
       }
       return false;
   }
   private static boolean checkStudentId(int id) {
       for(Student st :studentList) {
           if(st.getStudentId()==id) {
               return true;
           }
       }
       return false;
   }

}


public class Student {
   public int studentId;
   public String surname;
   public String lastName;
   public int age;
   public String sex;
  
   public Student(int studentId, String surname, String lastName, int age, String sex) {
      
       this.studentId = studentId;
       this.surname = surname;
       this.lastName = lastName;
       this.age = age;
       this.sex = sex;
   }

   public int getStudentId() {
       return studentId;
   }

   public void setStudentId(int studentId) {
       this.studentId = studentId;
   }

   public String getSurname() {
       return surname;
   }

   public void setSurname(String surname) {
       this.surname = surname;
   }

   public String getLastName() {
       return lastName;
   }

   public void setLastName(String lastName) {
       this.lastName = lastName;
   }

   public int getAge() {
       return age;
   }

   public void setAge(int age) {
       this.age = age;
   }

   public String getSex() {
       return sex;
   }

   public void setSex(String sex) {
       this.sex = sex;
   }
  
  

}



public class Book {

   public int bookId;
   public String tittle;
   public String authorSurname;
   public String authorFirsName;
   public String purchaseDate;
   public String status;
  
   public Book(int bookId, String tittle, String authorSurname, String authorFirsName, String purchaseDate,
           String status) {
      
       this.bookId = bookId;
       this.tittle = tittle;
       this.authorSurname = authorSurname;
       this.authorFirsName = authorFirsName;
       this.purchaseDate = purchaseDate;
       this.status = status;
   }

   public int getBookId() {
       return bookId;
   }

   public void setBookId(int bookId) {
       this.bookId = bookId;
   }

   public String getTittle() {
       return tittle;
   }

   public void setTittle(String tittle) {
       this.tittle = tittle;
   }

   public String getAuthorSurname() {
       return authorSurname;
   }

   public void setAuthorSurname(String authorSurname) {
       this.authorSurname = authorSurname;
   }

   public String getAuthorFirsName() {
       return authorFirsName;
   }

   public void setAuthorFirsName(String authorFirsName) {
       this.authorFirsName = authorFirsName;
   }

   public String getPurchaseDate() {
       return purchaseDate;
   }

   public void setPurchaseDate(String purchaseDate) {
       this.purchaseDate = purchaseDate;
   }

   public String getStatus() {
       return status;
   }

   public void setStatus(String status) {
       this.status = status;
   }
  
  
}