/**
 * 
 */
package src;

/**
 * @author xlivc
 *
 */
public class Calendar {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//import calendar package

		import java.util.Calendar;

		    //declare months in array

		String m[] = {

		"Jan", "Feb", "Mar", "Apr",

		"May", "Jun", "Jul", "Aug",

		"Sep", "Oct", "Nov", "Dec"};

		//create instance

		Calendar c = Calendar.getInstance();

		//Getting information from calendar

		System.out.print("Date: ");

		System.out.print(m[c.get(Calendar.MONTH)]);

		System.out.print(" " + c.get(Calendar.DATE) + " ");

		System.out.println(c.get(Calendar.YEAR));

		System.out.print("Time: ");

		System.out.print(c.get(Calendar.HOUR) + ":");

		System.out.print(c.get(Calendar.MINUTE) + ":");

		System.out.println(c.get(Calendar.SECOND));

		//setting values to calendar value

		c.set(Calendar.HOUR, 11);

		c.set(Calendar.MINUTE, 29);

		c.set(Calendar.SECOND, 22);

		System.out.print("Updated time: ");

		System.out.print(c.get(Calendar.HOUR) + ":");

		System.out.print(c.get(Calendar.MINUTE) + ":");

		System.out.println(c.get(Calendar.SECOND));

		}

		}
		

	}

}
